package com.wujunshen;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
@Slf4j
public class Eureka {
    public static void main(String[] args) {
        log.info("start execute Eureka....\n");
        SpringApplication.run(Eureka.class, args);
        log.info("end execute Eureka....\n");
    }
}